function Messenger_efx()
{
}

//config {direction:left,message:"text go here\nbreaking",style:style}
Messenger_efx.bubbleDraw = function(config)
{
    var padding = 10;
    var radius = 8;
    var bubble = config.sprite;
    var bubbleText = config.game.make.text(0,0,config.message,config.style);


    bubble.addChild(Messenger_efx.shapeBubble(bubbleText.width+(radius*4),bubbleText.height+(radius),config.direction,radius,bubble,config.game));
    bubble.addChild(bubbleText);
    var bubbleHeight = Number(bubbleText.height+radius)+padding;


    bubbleText.smoothed = true;
    bubble.smoothed = false;
    bubbleText.x = (radius*2)+10;
    bubbleText.y = radius+7;
    bubble.x = 40;
    if(config.direction == "left")
    {
        //bubbleText.fill = "#181818";
       // var head = config.game.make.sprite(0,bubbleText.height,'head');
        //bubble.addChild(head);
    }
    else
    {

        //bubble.x = 270-bubbleText.width;
        //bubbleText.fill = "#f8f8f8";
    }

    //config.sprite.add(bubble);



    /*
    var tw = this.game.add.tween(this.message_zone).to({y:this.message_zone.y-bubbleHeight-radius},350,Phaser.Easing.Quadratic.Out);

    tw.start();

    this.bubbleCount++;

    this.message_zone.smoothed = true;
    bubbleText.x = Math.round(bubbleText.x);
    return
    */
        
}
Messenger_efx.shapeBubble = function(width,height,direction,radius,parent,game)
{
    var corner = radius*2;
            var bubbleColor = 0x4267b2;
            var bubbleStroke = 0xffffff;
            var leftBottomCorner = corner;
            var rightBottomCorner = width 
            var rightTopCorner = corner+radius;
            switch(direction)
            {
                    /*
                case "left":
                    
                    //leftBottomCorner = corner+radius;
                break
                case "right":
                    //bubbleColor = 0x0284fe;
                     rightBottomCorner = width+radius

                break
                */
                case "topLeftCut":
                    rightTopCorner = corner;
                break
                case "bottomRightCut":
                    rightBottomCorner = width+radius;
                break

            }

            var graphics = game.add.graphics(0,0);
            graphics.lineStyle(2, bubbleStroke,2);
            graphics.beginFill(bubbleColor);
            graphics.moveTo(corner,corner);
            graphics.quadraticCurveTo(corner, radius,rightTopCorner,radius);
            graphics.lineTo(width,radius);
            graphics.quadraticCurveTo(width+radius, radius, width+radius,corner);
            graphics.lineTo(width+radius,height);
            graphics.quadraticCurveTo(width+radius, height+radius, rightBottomCorner,height+radius);
            graphics.lineTo(corner+radius,height+radius);
            graphics.quadraticCurveTo(corner,height+radius,corner,height);
            graphics.lineTo(corner,corner);

            return graphics
}
